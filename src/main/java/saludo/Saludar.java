package saludo;

public class Saludar {
	private String _nombre;
	
	public Saludar(String nombre)
	{
		_nombre= nombre;
	}
	public String saludar() {
		return "Hola "+_nombre+" "+"Buenas noches!";
	}

}
